<?php

namespace TantraGyan\PressCMS\Console;

use Illuminate\Console\Command;

class MigrationCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'press:migration';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a migration following the blog specifications.';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->laravel->view->addNamespace('press', substr(__DIR__, 0, -8) . 'views');

        $blogsTable = "presses";

        $this->line('');
        $this->info("Tables: $blogsTable");

        $message = "A migration that creates '$blogsTable' " .
            " tables will be created in database/migrations directory";

        $this->comment($message);
        $this->line('');

        $this->line('');

        $this->info("Creating migration...");

        if ($this->createMigration($blogsTable)) {

            $this->info("Migration successfully created!");
        } else {
            $this->error(
                "Couldn't create migration.\n Check the write permissions" .
                " within the database/migrations directory. \n" .
                " Or migration already exists. \n"
            );
        }

        if ($this->blogsAlterMigration($blogsTable)) {

            $this->info("Migration successfully created!");
        } else {
            $this->error(
                "Couldn't create migration.\n Check the write permissions" .
                " within the database/migrations directory. \n" .
                " Or migration already exists. \n"
            );
        }

        if ($this->blogsVideoAlterMigration($blogsTable)) {

            $this->info("Migration successfully created!");
        } else {
            $this->error(
                "Couldn't create migration.\n Check the write permissions" .
                " within the database/migrations directory. \n" .
                " Or migration already exists. \n"
            );
        }

        /*if ($this->blogsShortDescAlterMigration($blogsTable)) {

        $this->info("Migration successfully created!");
        } else {
        $this->error(
        "Couldn't create migration.\n Check the write permissions" .
        " within the database/migrations directory. \n" .
        " Or migration already exists. \n"
        );
        }

        if ($this->blogsStatusAlterMigration($blogsTable)) {

        $this->info("Migration successfully created!");
        } else {
        $this->error(
        "Couldn't create migration.\n Check the write permissions" .
        " within the database/migrations directory. \n" .
        " Or migration already exists. \n"
        );
        }*/

        $this->line('');
    }

    /**
     * Create the migration.
     *
     * @param string $name
     *
     * @return bool
     */
    protected function createMigration($blogsTable)
    {
        $migrationFile = base_path("/database/migrations") . "/2020_11_27_120560_presses_tables.php";

        $data = compact('blogsTable');

        $output = $this->laravel->view
            ->make('press::generators.migration')
            ->with($data)
            ->render();

        if (!file_exists($migrationFile) && $fs = fopen($migrationFile, 'x')) {
            fwrite($fs, $output);
            fclose($fs);
            return true;
        }

        return false;
    }
    protected function blogsAlterMigration($blogsTable)
    {
        $migrationFile = base_path("/database/migrations") . "/2020_12_08_110443_alter_presses_table.php";

        $data = compact('blogsTable');

        $output = $this->laravel->view
            ->make('press::generators.alter_presses_table')
            ->with($data)
            ->render();

        if (!file_exists($migrationFile) && $fs = fopen($migrationFile, 'x')) {
            fwrite($fs, $output);
            fclose($fs);
            return true;
        }

        return false;
    }

    protected function blogsVideoAlterMigration($blogsTable)
    {
        $migrationFile = base_path("/database/migrations") . "/2020_12_08_111443_add_video_link_to_presses_table.php";

        $data = compact('blogsTable');

        $output = $this->laravel->view
            ->make('press::generators.add_video_link_to_presses_table')
            ->with($data)
            ->render();

        if (!file_exists($migrationFile) && $fs = fopen($migrationFile, 'x')) {
            fwrite($fs, $output);
            fclose($fs);
            return true;
        }

        return false;
    }

}
