<?php echo '<?php' ?>


use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class AddVideoLinkToPressesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('presses', function ($table) {
			 $table->text('slug')->nullable()->change();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('presses', function ($table) {
			$table->dropColumn('slug');
		});
	}
}
