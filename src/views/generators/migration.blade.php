<?php echo '<?php' ?>

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PressesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('title')->nullable();
            $table->string('slug')->unique();
            $table->longText('short_description')->nullable();
            $table->longText('long_description')->nullable();
            $table->text('press_release_link')->nullable();
            $table->text('press_link')->nullable();
            $table->text('press_detail_image')->nullable();
            $table->text('press_detail_hash_name')->nullable();
            $table->text('press_list_image')->nullable();
            $table->text('press_list_hash_name')->nullable();
            $table->string('press_label1')->nullable();
            $table->string('press_label2')->nullable();
            $table->string('press_label3')->nullable();
            $table->string('contact_person_1_name')->nullable();
            $table->string('contact_person_1_email')->nullable();
            $table->string('contact_person_1_designation')->nullable();
            $table->string('contact_person_1_phone_number')->nullable();
            $table->string('contact_person_2_name')->nullable();
            $table->string('contact_person_2_designation')->nullable();
            $table->string('contact_person_2_email')->nullable();
            $table->string('contact_person_2_phone_number')->nullable();

            $table->string('meta_title')->nullable();
            $table->string('meta_keyword')->nullable();
            $table->text('meta_description')->nullable();
            $table->text('video_url')->nullable();
            $table->string('og_title')->nullable();
            $table->string('og_url')->nullable();
            $table->string('og_type')->nullable();
            $table->text('og_image_content')->nullable();
            $table->text('og_description')->nullable();
            $table->string('twitter_title')->nullable();
            $table->string('twitter_card')->nullable();
            $table->text('twitter_url')->nullable();
            $table->text('twitter_image')->nullable();
            $table->text('twitter_description')->nullable();
            $table->string('canonical_url')->nullable();
            $table->text('description_content')->nullable();
            $table->string('itemprop_name')->nullable();
            $table->text('itemprop_image')->nullable();
            $table->longText('itemprop_description')->nullable();
            $table->timestamp('published_date')->nullable();

            $table->string('published_status')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presses');
    }
}
