<?php

namespace TantraGyan\PressCMS\Jobs\Press;

use File;
use Illuminate\Http\Request;
use Image;
use TantraGyan\PressCMS\Jobs\Job;
use TantraGyan\PressCMS\Models\Press;

class UpdatePressJob extends Job
{
    /**
     * @var Object
     */
    private $press;
    private $id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
        $press = Press::where('id', $this->id)->first();

        $id                    = $press->id;
        $bannerImageOriginal   = null;
        $bannerMobileImageName = null;
        //dd($press);
        if (request()->press_image_crop) {
            $image_to_delete = $press->press_detail_image;
            if (File::exists($image_to_delete)) {
                File::delete($image_to_delete);
            }

            $mobile_image_to_delete = $press->press_list_image;
            if (File::exists($mobile_image_to_delete)) {
                File::delete($mobile_image_to_delete);
            }

            if (request()->press_detail_image) {
                $directoryThumbName = "uploads/PressCMS/press-detail-image/";
                if (!is_dir($directoryThumbName)) {
                    mkdir($directoryThumbName, 0755, true);
                }

                $directoryCoverName = "uploads/PressCMS/press-list-images/";
                if (!is_dir($directoryCoverName)) {
                    mkdir($directoryCoverName, 0755, true);
                }

                $news_image = request()->press_detail_image;

                $news_image = str_replace('data:image/png;base64,', '', $news_image);
                $news_image = str_replace(' ', '+', $news_image);
                $imageName  = "press" . time() . str_random(10) . '.' . 'png';

                $news_image_new_name = "uploads/PressCMS/press-detail-image/" . $imageName;

                $newsImagePath = File::put($news_image_new_name, base64_decode($news_image));

                //news_list_image
                $newsListImage = request()->press_detail_image;

                $listImage = Image::make($newsListImage);

                $listImageName = 'pressList-' . time() . str_random(10) . '.' . 'png';

                $listImagePath = "uploads/PressCMS/press-list-images/" . $listImageName;

                $listImage->resize(100, 60);

                $listImage->save("uploads/PressCMS/press-list-images/" . $listImageName);

                $press->press_detail_image = $news_image_new_name;
                $press->press_list_image   = $listImagePath;

            }
        }

        $press->id                            = $id;
        $press->title                         = request()->title;
        $press->long_description              = request()->long_description;
        $press->short_description             = request()->short_description;
        $press->press_release_link            = request()->press_release_link;
        $press->press_label1                  = request()->press_label1;
        $press->press_label2                  = request()->press_label2;
        $press->press_label3                  = request()->press_label3;
        $press->contact_person_1_name         = request()->contact_person_1_name;
        $press->contact_person_1_designation  = request()->contact_person_1_designation;
        $press->contact_person_1_phone_number = request()->contact_person_1_phone_number;
        $press->contact_person_1_phone_number = request()->contact_person_1_phone_number;
        $press->contact_person_1_email        = request()->contact_person_1_email;
        $press->contact_person_2_name         = request()->contact_person_2_name;
        $press->contact_person_2_designation  = request()->contact_person_2_designation;
        $press->contact_person_2_phone_number = request()->contact_person_2_phone_number;
        $press->contact_person_2_email        = request()->contact_person_2_email;
        $press->meta_title                    = request()->meta_title;
        $press->meta_keyword                  = request()->meta_keyword;
        $press->meta_description              = request()->meta_description;
        $press->video_url                     = request()->video_url;
        $press->og_title                      = request()->og_title;
        $press->og_url                        = request()->og_url;
        $press->og_type                       = request()->og_type;
        $press->og_image_content              = request()->og_image_content;
        $press->og_description                = request()->og_description;
        $press->twitter_title                 = request()->twitter_title;
        $press->twitter_card                  = request()->twitter_card;
        $press->twitter_url                   = request()->twitter_url;
        $press->twitter_image                 = request()->twitter_image;
        $press->twitter_description           = request()->twitter_description;
        $press->canonical_url                 = request()->canonical_url;
        $press->description_content           = request()->description_content;
        $press->itemprop_name                 = request()->itemprop_name;
        $press->itemprop_image                = request()->itemprop_image;
        $press->itemprop_description          = request()->itemprop_description;
        $press->published_date          = request()->published_date;
        $press->save();

        return $press;

    }
}
