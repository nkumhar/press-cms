<?php

namespace TantraGyan\PressCMS\Jobs\Press;

use TantraGyan\PressCMS\Jobs\Job;
use TantraGyan\PressCMS\Models\Press;

class GetPressJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Press $press)
    {
        return $press->get();
    }
}
