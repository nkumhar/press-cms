<?php

namespace TantraGyan\PressCMS\Jobs\Press;

use TantraGyan\PressCMS\Filters\Press\PressFilter;
use TantraGyan\PressCMS\Jobs\Job;
use TantraGyan\PressCMS\Models\Press;

class PaginatePressJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */

    public function handle(Press $press, PressFilter $filter)
    {
        $request = request();

        // $sortBy = $request->sort_by ?? 'created_at';
        $sortBy = $request->sort_by ?? 'published_date';

        if ($request) {
            if ($request->input('keyword')) {
                $press_keyword = $request->keyword;
                $press         = $press->where(function ($q) use ($press_keyword) {
                    $q->where('title', 'like', '%' . $press_keyword . '%');
                    $q->orWhere('long_description', 'like', '%' . $press_keyword . '%');
                    $q->orWhere('short_description', 'like', '%' . $press_keyword . '%');
                });
            }
        }

        $releases = $press->where('published_status', 1);
        $releases = $releases->latest($sortBy)
            ->filter($filter)
            ->customSortable();

        /*if ($request->publish) {
        $press = $press->whereNull('published_status')
        ->where('created_at', '<=', $today);
        }*/
        /*if ($request->publish) {
        $releases = $releases->where('created_at', '<=', $today);
        }*/
        return $releases->paginate($request->limit ?? 10);
    }
}
