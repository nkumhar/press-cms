<?php

namespace TantraGyan\PressCMS\Jobs\Press;

use File;
use Image;
use TantraGyan\PressCMS\Jobs\Job;
use TantraGyan\PressCMS\Models\Press;

/**
 *
 */
class StorePressJob extends Job
{
    /**
     * @var Object
     */
    private $pressObject;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public function __construct($pressObject = null)
    {
        $this->pressObject = $pressObject;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Press $press)
    {
        $press                = new Press();
        $id                   = request()->id;
        $pressDetailImageName = null;
        $pressDetailHashName  = null;
        $pressListImageName   = null;
        $pressListHashName    = null;

        $directoryThumbName = "uploads/PressCMS/press-detail-image/";
        if (!is_dir($directoryThumbName)) {
            mkdir($directoryThumbName, 0755, true);
        }

        $directoryCoverName = "uploads/PressCMS/press-list-images/";
        if (!is_dir($directoryCoverName)) {
            mkdir($directoryCoverName, 0755, true);
        }

        if (request()->press_detail_image) {
            //news_detail_image

            $news_image = request()->press_detail_image;

            $news_image = str_replace('data:image/png;base64,', '', $news_image);
            $news_image = str_replace(' ', '+', $news_image);
            $imageName  = "press" . time() . str_random(10) . '.' . 'png';

            $news_image_new_name = "uploads/PressCMS/press-detail-image/" . $imageName;

            $newsImagePath = File::put($news_image_new_name, base64_decode($news_image));

            //news_list_image
            $newsListImage = request()->press_detail_image;

            $listImage = Image::make($newsListImage);

            $listImageName = 'pressList-' . time() . str_random(10) . '.' . 'png';

            $listImagePath = "uploads/PressCMS/press-list-images/" . $listImageName;

            $listImage->resize(100, 60);

            $listImage->save("uploads/PressCMS/press-list-images/" . $listImageName);

        }

        return $press->updateOrCreate(
            [
                'id' => $id,
            ],
            [
                'title'                         => request()->title,
                'long_description'              => request()->long_description,
                'short_description'             => request()->short_description,
                'press_release_link'            => request()->press_release_link,
                'press_link'                    => request()->press_link,
                'press_detail_image'            => $news_image_new_name,
                //'press_detail_hash_name'        => $pressDetailHashName,
                'press_list_image'              => $listImagePath,
                //'press_list_hash_name'          => $pressDetailHashName,
                'press_label1'                  => request()->press_label1,
                'press_label2'                  => request()->press_label2,
                'press_label3'                  => request()->press_label3,
                'contact_person_1_name'         => request()->contact_person_1_name,
                'contact_person_1_designation'  => request()->contact_person_1_designation,
                'contact_person_1_phone_number' => request()->contact_person_1_phone_number,
                'contact_person_1_email'        => request()->contact_person_1_email,
                'contact_person_2_name'         => request()->contact_person_2_name,
                'contact_person_2_designation'  => request()->contact_person_2_designation,
                'contact_person_2_phone_number' => request()->contact_person_2_phone_number,
                'contact_person_2_email'        => request()->contact_person_2_email,
                'meta_title'                    => request()->meta_title,
                'meta_keyword'                  => request()->meta_keyword,
                'meta_description'              => request()->meta_description,
                'video_url'                     => request()->video_url,
                'og_title'                      => request()->og_title,
                'og_url'                        => request()->og_url,
                'og_type'                       => request()->og_type,
                'og_image_content'              => request()->og_image_content,
                'og_description'                => request()->og_description,
                'twitter_title'                 => request()->twitter_title,
                'twitter_card'                  => request()->twitter_card,
                'twitter_url'                   => request()->twitter_url,
                'twitter_image'                 => request()->twitter_image,
                'twitter_description'           => request()->twitter_description,
                'canonical_url'                 => request()->canonical_url,
                'description_content'           => request()->description_content,
                'itemprop_name'                 => request()->itemprop_name,
                'itemprop_image'                => request()->itemprop_image,
                'itemprop_description'          => request()->itemprop_description,
                'published_status'              => request()->published_status == true ? 1 : null,
                'published_date'                => request()->published_date,

            ]);
    }
}
