<?php

namespace TantraGyan\PressCMS\Controllers\Press;

use Carbon\Carbon;
use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Serializer\ArraySerializer;
use TantraGyan\PressCMS\Controllers\ApiController;
use TantraGyan\PressCMS\Jobs\Press\GetPressJob;
use TantraGyan\PressCMS\Jobs\Press\PaginatePressJob;
use TantraGyan\PressCMS\Jobs\Press\StorePressJob;
use TantraGyan\PressCMS\Jobs\Press\UpdatePressJob;
use TantraGyan\PressCMS\Models\Press;
use TantraGyan\PressCMS\Requests\Press\StorePressRequest;
use TantraGyan\PressCMS\Requests\Press\UpdatePressRequest;
use TantraGyan\PressCMS\Transformers\Press\PressTransformer;

class PressController extends ApiController
{
    public function master(Request $request)
    {
        try {
            $press = $this->dispatch(new GetPressJob());

            if ($press->isEmpty()) {

                return $this->respondNotFound("Records not found.");
            }

            $response = fractal()
                ->collection($press, new PressTransformer())
                ->parseIncludes(['created_at', 'published_date'])
                ->serializeWith(new ArraySerializer())
                ->toArray();

            return $this->setMessage("Press fetched successfully.")
                ->respondWithStatus(['data' => $response]);

        } catch (\Exception $e) {

            \Log::error($e->getMessage());

            return $this->respondInternalError("Something went wrong!");
        }
    }
    public function index()
    {
        try {

            $press = $this->dispatch(new PaginatePressJob());
            if ($press->isEmpty()) {

                return $this->respondNotFound("Records not found.");
            }

            $pressResponse = fractal()
                ->collection($press, new PressTransformer())
                ->parseIncludes(['created_at', 'published_date'])
                ->serializeWith(new ArraySerializer())
                ->paginateWith(new IlluminatePaginatorAdapter($press))
                ->toArray();

            $response = $this->convertPaginationResponse($pressResponse);

            return $this->setMessage("Press Release fetched successfully.")
                ->respondWithStatus($response);
        } catch (\Exception $e) {

            \Log::error($e->getMessage());

            return $this->respondInternalError("Something went wrong!");
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePressRequest $request)
    {
        try {
            $press = $this->dispatch(new StorePressJob());
            if ($press) {

                $pressResponse = fractal()
                    ->item($press, new PressTransformer())
                    ->toArray();

                return $this->setMessage('Press added successfully.')
                    ->respondWithStatus(['data' => $pressResponse]);

            }

            return $this->respondInternalError("Something went wrong.");

        } catch (\Exception $e) {

            \Log::error($e->getMessage());

            return $this->respondInternalError("Something went wrong.");
        }
    }

    public function update(UpdatePressRequest $request)
    {
        try {
            $id    = $request->id;
            $press = $this->dispatch(new UpdatePressJob($id));
            if ($press) {

                $pressResponse = fractal()
                    ->item($press, new PressTransformer())
                    ->toArray();

                return $this->setMessage('Press added successfully.')
                    ->respondWithStatus(['data' => $pressResponse]);

            }

            return $this->respondInternalError("Something went wrong.");

        } catch (\Exception $e) {

            \Log::error($e->getMessage());

            return $this->respondInternalError("Something went wrong.");
        }
    }

    public function show($slug)
    {
        try {

            $pressshow = Press::where('slug', $slug)->where('published_status', 1)->first();
            if ($pressshow) {

                $pressResponse = fractal()
                    ->item($pressshow, new PressTransformer())
                    ->parseIncludes(['created_at', 'published_date'])
                    ->serializeWith(new ArraySerializer())
                    ->toArray();

                return $this->setMessage('Press Release fetched successfully.')
                    ->respondWithStatus(['data' => $pressResponse]);

            }

            return $this->respondNotFound("Record not found.");

        } catch (\Exception $e) {

            \Log::error($e->getMessage());

            return $this->respondInternalError("Something went wrong.");
        }
    }

    public function publishStatus(Request $request)
    {

        $press = Press::find($request->id);

        if ($request->published_status) {
            $press->update(['published_status' => 1]);
            // $press->update(['published_date' => Carbon::now()]);
            $press->save();

            return response()->json(['success' => 'Publish Status change successfully to publish.']);
        } else {
            $press->update(['published_status' => null]);
            // $press->update(['published_date' => null]);

            $press->save();

            return response()->json(['success' => 'Publish Status change successfully to unpublish.']);
        }

    }

    public function destroy($id)
    {
        try {
            $press = Press::find($id);
            if ($press) {

                $press->delete();

                return $this->setMessage('Press deleted successfully.')
                    ->respondWithStatus();
            }

            return $this->respondInternalError("Something went wrong.");

        } catch (\Exception $e) {

            \Log::error($e->getMessage());

            return $this->respondInternalError("Something went wrong.");
        }
    }

}
