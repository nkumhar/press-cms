<?php

namespace TantraGyan\PressCMS\Requests\Press;

use TantraGyan\PressCMS\Requests\Request;

class StorePressRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title"                     => "required",
            /*"press_detail_image"        => "required|image",
            "press_list_image"          => "required|image",*/
            "long_description"    => "required",
            "short_description"   => "required",
            "published_date"   => "required",

        ];
    }
}
