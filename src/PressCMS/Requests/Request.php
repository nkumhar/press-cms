<?php

namespace TantraGyan\PressCMS\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class Request extends FormRequest
{
    //

    public function wantsJson()
    {
        // echo 'hi';exit;
        if (request()->ajax()) {
            return true;
        }
        if (Request::is('api/*')) {
            return true;
        }
    }
}
