<?php

namespace TantraGyan\PressCMS\Transformers;

use League\Fractal\TransformerAbstract;

class DateTransformer extends TransformerAbstract
{
  /**
   * List of resources to automatically include
   *
   * @var array
   */
  protected $defaultIncludes = [
      //
  ];

  /**
   * List of resources possible to include
   *
   * @var array
   */
  protected $availableIncludes = [
      //
  ];

  /**
   * A Fractal transformer.
   *
   * @return array
   */
  public function transform($created_at)
  {
      $formatted_date = date("M d, Y", strtotime($created_at));
      $nonformatted_date = $created_at->format('d-m-Y');
      $created_at_time = $created_at->format('H:i A');
      $system_date = $nonformatted_date." ".$created_at_time;

      return [
        'system_date'            => $system_date,
        'formatted_date'          => $formatted_date,
        'formatted_time'          => $created_at_time,
        'nonformatted_date'         => $nonformatted_date
      ];
  }
}
