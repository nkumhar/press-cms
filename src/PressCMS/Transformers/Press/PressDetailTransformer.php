<?php

namespace TantraGyan\PressCMS\Transformers\Press;

use League\Fractal\TransformerAbstract;
use TantraGyan\PressCMS\Models\Press;
use TantraGyan\PressCMS\Transformers\DateTransformer;

class PressDetailTransformer extends TransformerAbstract
{
  /**
   * List of resources to automatically include
   *
   * @var array
   */
  protected $defaultIncludes = [

  ];

  /**
   * List of resources possible to include
   *
   * @var array
   */
  protected $availableIncludes = [
    'created_at','published_date',
  ];

  /**
   * A Fractal transformer.
   *
   * @return array
   */
  public function transform(Press $press)
  {

    return [
      'id'                      => $press->id,
                'title'                   => $press->title,
                'slug'                    => $press->slug,
                'long_description'  => $press->long_description,
                'short_description' => $press->short_description,
                "press_link"              => $press->press_link,
                'press_detail_image'            => $item->press_detail_image ? url($item->press_detail_image) : null,
                'press_list_image'              => $item->press_list_image ? url($item->press_list_image) : null,
                'press_release_link'      => $press->press_release_link,
                'label_1'                 => $press->press_label1,
                'label_2'                 => $press->press_label2,
                'label_3'                 => $press->press_label3,
                'meta_title'              => $press->meta_title,
                'meta_keyword'            => $press->meta_keyword,
                'meta_description'        => $press->meta_description,
                'og_title'                    => $press->og_title,
                'og_url'                      => $press->og_url,
                'og_type'                     => $press->og_type,
                'og_image_content'            => $press->og_image_content,
                'og_description'              => $press->og_description,
                'twitter_title'               => $press->twitter_title,
                'twitter_card'                => $press->twitter_card,
                'twitter_url'                 => $press->twitter_url,
                'twitter_image'               => $press->twitter_image,
                'twitter_description'         => $press->twitter_description,
                'canonical_url'               => $press->canonical_url,
                'description_content'         => $press->description_content,
                'itemsprop_name'               => $press->itemprop_name,
                'itemprop_image'              => $press->itemprop_image,
                'itemprop_description'        => $press->itemprop_description,
    ];

  }

  public function includeCreatedAt(Press $press)
  {
    $created_at = $press->created_at;

    return $created_at ? $this->item($created_at,new DateTransformer()) : null;
  }
  public function includePublishedDate(Press $press)
  {
    $created_at = $press->published_date;

    return $created_at ? $this->item($created_at,new DateTransformer()) : null;
  }

}
