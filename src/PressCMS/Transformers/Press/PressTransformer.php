<?php

namespace TantraGyan\PressCMS\Transformers\Press;

use League\Fractal\TransformerAbstract;
use TantraGyan\PressCMS\Models\Press;
use TantraGyan\PressCMS\Transformers\DateTransformer;

class PressTransformer extends TransformerAbstract
{

    protected $availableIncludes = ['created_at', 'published_date',
    ];

    public function transform($item)
    {
        if ($item) {

            return [
                'id'                            => $item->id,
                'title'                         => $item->title,
                'slug'                          => $item->slug,
                'long_description'              => $item->long_description,
                'short_description'             => $item->short_description,
                "press_link"                    => $item->press_link,
                'press_detail_image'            => $item->press_detail_image ? url($item->press_detail_image) : null,
                'press_list_image'              => $item->press_list_image ? url($item->press_list_image) : null,

                'press_release_link'            => $item->press_release_link,
                'label_1'                       => $item->press_label1,
                'label_2'                       => $item->press_label2,
                'label_3'                       => $item->press_label3,
                'meta_title'                    => $item->meta_title,
                'meta_keyword'                  => $item->meta_keyword,
                'meta_description'              => $item->meta_description,
                'video_url'                     => $item->video_url,
                'og_title'                      => $item->og_title,
                'og_url'                        => $item->og_url,
                'og_type'                       => $item->og_type,
                'og_image_content'              => $item->og_image_content,
                'og_description'                => $item->og_description,
                'twitter_title'                 => $item->twitter_title,
                'twitter_card'                  => $item->twitter_card,
                'twitter_url'                   => $item->twitter_url,
                'twitter_image'                 => $item->twitter_image,
                'twitter_description'           => $item->twitter_description,
                'canonical_url'                 => $item->canonical_url,
                'description_content'           => $item->description_content,
                'itemprop_name'                 => $item->itemprop_name,
                'itemprop_image'                => $item->itemprop_image,
                'itemprop_description'          => $item->itemprop_description,
                'contact_person_1_name'         => $item->contact_person_1_name,
                'contact_person_1_designation'  => $item->contact_person_1_designation,
                'contact_person_1_email'        => $item->contact_person_1_email,
                'contact_person_1_phone_number' => $item->contact_person_1_phone_number,
                'contact_person_2_name'         => $item->contact_person_2_name,
                'contact_person_2_designation'  => $item->contact_person_2_designation,
                'contact_person_2_phone_number' => $item->contact_person_2_phone_number,
                'contact_person_2_email'        => $item->contact_person_2_email,

            ];
        }
        return [];
    }

    public function includeCreatedAt(Press $press)
    {
        $created_at = $press->created_at;

        return $created_at ? $this->item($created_at, new DateTransformer()) : null;
    }

    public function includePublishedDate(Press $press)
    {
        $created_at = $press->published_date;

        return $created_at ? $this->item($created_at, new DateTransformer()) : null;
    }
}
