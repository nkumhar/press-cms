<?php

//  Banner routes
Route::group(['prefix' => 'api', 'middleware' => 'web'], function () {

    Route::group(['prefix' => 'v1'], function () {

        Route::get('/presses', 'Press\PressController@index')
            ->name('api.press.index');

        Route::post('presses/store', 'Press\PressController@store')
            ->name('api.press.store');
            // ->middleware('auth');

        Route::post('presses/update', 'Press\PressController@update')
            ->name('api.press.update');
            // ->middleware('auth');

        Route::get('presses/{slug}', 'Press\PressController@show')
            ->name('api.press.show');

        Route::delete('presses/{id}', 'Press\PressController@destroy')
            ->name('api.press.destroy');
            // ->middleware('auth');

    });

    Route::group(['prefix' => 'v1/press-master'], function () {

        Route::get('/', 'Press\PressController@master')
            ->name('api.press-master.index');
    });

    Route::get('/v1/publishPressStatus', 'Press\PressController@publishStatus')
        ->name('api.press.publishPressStatus');
    /*->middleware('auth');*/

});

Route::group(['middleware' => 'web'], function () {

    Route::get('press', [
        'as'   => 'press',
        'uses' => 'Press\PressController@showBlogPage',
    ]);
});
