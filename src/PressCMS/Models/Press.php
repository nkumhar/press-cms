<?php

namespace TantraGyan\PressCMS\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use TantraGyan\PressCMS\Filters\QueryFilter;

class Press extends Model
{
    use SoftDeletes,
    Sluggable,
        Sortable;

    protected $guarded = ['id'];

    protected $dates = ['published_date'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title',
            ],
        ];
    }

    /*
    |--------------------------------------------------------------------------
    | Sortable trait
    |--------------------------------------------------------------------------
    |
     */

    public $sortable = [
        "title",
        "press_short_description",
        "created_at",
        "updated_at",
    ];

    /**
     * Custom options for sort keys
     *
     *@return Collection
     */
    public static function customSorts()
    {
        return collect([
            'title'                   => 'title',
            'press_short_description' => 'press_short_description',
            'created_at'              => 'created_at',
            'updated_at'              => 'updated_at',
        ]);
    }

    public function scopeCustomSortable($query)
    {
        $request = request();
        $sort    = $request->sort ? $this->customSorts()->get($request->sort) : 'updated_at';
        $order   = $request->order ? $request->order : 'desc';

        $request->request->add([
            'sort'  => $sort,
            'order' => $order,
        ]);

        return $query->sortable([$sort => $order]);
    }

    /*
    |--------------------------------------------------------------------------
    | Scope Queries
    |--------------------------------------------------------------------------
    |
     */

    /**
     * Filter scope for searching model using query filter
     *
     * @param  Builder      $query
     * @param  QueryFilter $filters
     * @return mixed
     */
    public function scopeFilter($query, QueryFilter $filters)
    {
        return $filters->apply($query);
    }

    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    |
     */

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function getDetailImagePathAttribute()
    {
        return $this->press_detail_image ? config('app.url') . '/uploads/PressCMS/press-detail-image/' . $this->press_detail_image : null;
    }

    public function getListImagePathAttribute()
    {
        return $this->press_list_image ? config('app.url') . '/uploads/PressCMS/press-list-images/' . $this->press_list_image : null;
    }
    public function getTwitterImagePathAttribute()
    {
        return $this->twitter_image ? config('app.url') . '/PressCMS/Twitter_images/' . $this->twitter_image : null;
    }

    public function getItempropImagePathAttribute()
    {
        return $this->itemprop_image ? config('app.url') . '/PressCMS/Itemprop_images/' . $this->itemprop_image : null;
    }
}
