<?php

namespace TantraGyan\PressCMS;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use TantraGyan\PressCMS\Console\MigrationCommand;

class PressServiceProvider extends ServiceProvider
{
	/**
     * Bootstrap the application services.
     *
     * @return void
     */
    protected $defer     = false;
    protected $namespace = 'TantraGyan\PressCMS\Controllers';

    public function boot()
    {
    	$this->mapBlogRoutes();
        $this->loadViewsFrom(__DIR__ . '/../views', 'press');
        $this->commands('command.press.migration'); // Register commands

        $this->publishes([__DIR__ . '/../assets/sass' => public_path("sass/pages"),
        ], 'public');

        $this->publishes([__DIR__ . '/../config/press.php' => config_path('press.php'),
        ], "config");
    }

    public function register()
    {
        $this->registerBlog();
        $this->registerCommands();
    }

    public function mapBlogRoutes()
    {

        Route::group(['middleware' => 'web', 'namespace' => $this->namespace],
            function ($router) {
                if (!$this->app->routesAreCached()) {
                    require __DIR__ . '/routes/routes.php';
                    //  $this->loadRoutesFrom(__DIR__ . '/routes/routes.php'); // 5.4
                }
            });
    }

    private function registerBlog()
    {
        $this->app->bind('press', function ($app) {
            return new Press($app);
        });

        $this->app->alias('press', 'TantraGyan\PressCMS');
    }

    private function registerCommands()
    {
        $this->app->singleton('command.press.migration', function ($app) {
            return new MigrationCommand();
        });
    }
    public function provides()
    {
        return [
            'command.press.migration',
        ];
    }



}