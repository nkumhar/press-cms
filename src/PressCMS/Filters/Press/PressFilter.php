<?php
namespace TantraGyan\PressCMS\Filters\Press;

use TantraGyan\PressCMS\Filters\QueryFilter;

class PressFilter extends QueryFilter
{

    public function title($value = null)
    {
        if ($value) {

            return $this->builder->where('title', 'LIKE', '%' . $value . '%');
        }
    }

    public function shortDescription($value = null)
    {
        if ($value) {

            return $this->builder->where('short_description', 'LIKE', '%' . $value . '%');
        }
    }

    public function longDescription($value = null)
    {
        if ($value) {

            return $this->builder->where('long_description', 'LIKE', '%' . $value . '%');
        }
    }

    public function year($value = null)
    {
        if ($value) {

            return $this->builder->whereYear('created_at', $value);
        }
    }

    public function month($value = null)
    {
        if ($value) {

            return $this->builder->whereMonth('created_at', $value);
        }
    }

    /*public function tags($value = null)
{
if ($value) {

return $this->builder->whereHas('tags', function ($tag) use ($value) {

$tag->where('name', 'LIKE', '%' . $value . '%');
});
}
}*/
}
