<?php
namespace TantraGyan\PressCMS\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

abstract class QueryFilter
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var
     */
    protected $builder;

    /**
     * QueryFilter constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Make builder queries for searching
     * append search conditions
     *
     * @param Builder $builder
     * @return Builder
     */
    public function apply(Builder $builder)
    {
        $this->builder = $builder;

        foreach ($this->filters() as $name => $value) {

            $functionName = camel_case($name);
            // if method for search parameter does exists and parameter value is not blank
            // then only call parameter method
            if (method_exists($this, $functionName) && $value != '') {
                call_user_func_array([$this, $functionName], array_filter([$value]));
            }
        }

        return $this->builder;
    }

    /**
     * Return all request parameters
     *
     * @return array
     */
    public function filters()
    {
        return $this->request->all();
    }
}
